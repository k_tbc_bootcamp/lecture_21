package com.lkakulia.lecture_21.http_request

interface CustomCallback {
    fun onFailure(error: String?)
    fun onResponse(response: String)
}