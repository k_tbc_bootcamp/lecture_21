package com.lkakulia.lecture_21.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lkakulia.lecture_21.activities.UserDashboardActivity
import com.lkakulia.lecture_21.models.UserModel
import com.lkakulia.lecture_21.R
import kotlinx.android.synthetic.main.user_recyclerview_layout.view.*

class RecyclerViewAdapter(private val usersData: ArrayList<UserModel.DataModel>): RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> () {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(
            R.layout.user_recyclerview_layout,
            parent,
            false
        ))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount() = usersData.size

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private lateinit var userData: UserModel.DataModel

        fun onBind() {
            userData = usersData[adapterPosition]

            itemView.setOnClickListener{
                val intent = Intent(itemView.context, UserDashboardActivity::class.java).apply {
                    putExtra("id", userData.id)
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                }

                itemView.context.startActivity(intent)
            }
            itemView.idTextView.text= userData.id.toString()
            itemView.nameTextView.text = userData.name
            itemView.yearTextView.text = userData.year.toString()
            itemView.colorTextView.text = userData.color
            itemView.pantoneValueTextView.text = userData.pantoneValue
        }
    }
}