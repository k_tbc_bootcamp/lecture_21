package com.lkakulia.lecture_21.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.gson.Gson
import com.lkakulia.lecture_21.models.EmployeeModel
import com.lkakulia.lecture_21.R
import kotlinx.android.synthetic.main.activity_employee_dashboard.*

class EmployeeDashboardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_dashboard)

        init()
    }

    private fun init() {
        val response = intent.getStringExtra("employeeData")
        val employee = Gson().fromJson(response, EmployeeModel::class.java)

        nameTextView.text = employee.name
        jobTextView.text = employee.job
        idTextView.text = employee.id.toString()
        createdAtTextView.text = employee.createdAt
    }
}
