package com.lkakulia.lecture_21.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.lkakulia.lecture_21.http_request.CustomCallback
import com.lkakulia.lecture_21.http_request.HttpRequest
import com.lkakulia.lecture_21.R
import kotlinx.android.synthetic.main.activity_create_employee.*

class CreateEmployeeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_employee)

        init()
    }

    private fun init() {
        createEmployeeButton.setOnClickListener({
            val name = nameEditText.text.toString()
            val job = jobEditText.text.toString()

            if (name.isNotEmpty() && job.isNotEmpty()) {
                val parameters = mutableMapOf<String, String>()
                parameters["name"] = name
                parameters["job"] = job

                HttpRequest.postRequest(
                    HttpRequest.USERS, parameters, object:
                        CustomCallback {
                    override fun onFailure(error: String?) {
                        Toast.makeText(this@CreateEmployeeActivity, error, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(response: String) {
                        val intent = Intent(this@CreateEmployeeActivity, EmployeeDashboardActivity::class.java).apply {
                            putExtra("employeeData", response)
                            setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        }
                        startActivity(intent)
                    }

                })
            }

            else {
                Toast.makeText(this, "Please fill in all the fields", Toast.LENGTH_SHORT).show()
            }
        })
    }
}
