package com.lkakulia.lecture_21.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.lkakulia.lecture_21.adapters.RecyclerViewAdapter
import com.lkakulia.lecture_21.http_request.CustomCallback
import com.lkakulia.lecture_21.http_request.HttpRequest
import com.lkakulia.lecture_21.models.UserModel
import com.lkakulia.lecture_21.R
import kotlinx.android.synthetic.main.activity_users_dashboard.*

class UsersDashboardActivity : AppCompatActivity() {

    private val usersData = ArrayList<UserModel.DataModel>()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users_dashboard)

        init()
    }

    private fun init() {

        HttpRequest.getRequest(
            HttpRequest.UNKNOWN, object :
                CustomCallback {
            override fun onFailure(error: String?) {
                Toast.makeText(this@UsersDashboardActivity, error, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(response: String) {
                usersData.addAll(Gson().fromJson(response, UserModel::class.java).data)
                d("usersData", "$usersData")
                recyclerView.layoutManager = LinearLayoutManager(this@UsersDashboardActivity)
                adapter = RecyclerViewAdapter(usersData)
                recyclerView.adapter = adapter
            }
        })

        createEmployeeButton.setOnClickListener{
            val intent = Intent(this, CreateEmployeeActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            }
            startActivity(intent)
        }

    }
}

