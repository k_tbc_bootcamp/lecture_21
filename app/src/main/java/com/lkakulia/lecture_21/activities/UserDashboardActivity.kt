package com.lkakulia.lecture_21.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.gson.Gson
import com.lkakulia.lecture_21.http_request.CustomCallback
import com.lkakulia.lecture_21.http_request.HttpRequest
import com.lkakulia.lecture_21.models.UserModel
import com.lkakulia.lecture_21.R
import kotlinx.android.synthetic.main.activity_user_dashboard.*
import org.json.JSONObject

class UserDashboardActivity : AppCompatActivity() {

    private lateinit var userData: UserModel.DataModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_dashboard)

        init()
    }

    private fun init() {
        val id = intent.getIntExtra("id", 0)

        HttpRequest.getRequest(
            HttpRequest.UNKNOWN, id, object:
                CustomCallback {
            override fun onFailure(error: String?) {
                Toast.makeText(this@UserDashboardActivity, error, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(response: String) {
                val userInfoJson = JSONObject(response)

                if (userInfoJson.has("data")) {
                    userData = Gson().fromJson(
                        userInfoJson.getString("data"),
                        UserModel.DataModel::class.java
                    )

                    idTextView.text = userData.id.toString()
                    nameTextView.text = userData.name
                    yearTextView.text = userData.year.toString()
                    colorTextView.text = userData.color
                    pantoneValueTextView.text = userData.pantoneValue
                }
            }

        })


    }
}
