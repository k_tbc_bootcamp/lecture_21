package com.lkakulia.lecture_21.models

import com.google.gson.annotations.SerializedName

class UserModel {
    var page = 0
    @SerializedName("per_page")
    var perPage = 0
    var total = 0
    @SerializedName("total_pages")
    var totalPages = 0
    var data = ArrayList<DataModel>()

    class DataModel {
        var id = 0
        var name = ""
        var year = 0
        var color = ""
        @SerializedName("pantone_value")
        var pantoneValue = ""
    }
}